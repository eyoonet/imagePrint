﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Xml;

namespace PrintImagProject
{
    public partial class FormSet : Form
    {
        private  PrintLibrary.PrintConfig config;
        private FileLibrary.FileWatcher file;

        public FormSet(ref PrintLibrary.PrintConfig config,ref FileLibrary.FileWatcher file)
        {
            this.config = config; this.file = file;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            textBoxPicPath.Text = folderBrowserDialog1.SelectedPath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            textBox1.Text = openFileDialog1.FileName;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //保存打印机驱动
            config.PrinterName = comboBox1.Text;
            config.Save();
            //保存监视路径
            if (textBoxPicPath.Text != string.Empty)
            {
                file.WatcherPath = textBoxPicPath.Text;
                file.SetWatcher();
                SetAppConfig("WatcherPath", textBoxPicPath.Text.Trim());
            }
            else 
            {
                MessageBox.Show("监视路径路径不能为空");
                return;
            }
            //保存MP3路径
            if (textBox1.Text != string.Empty) 
            {
                SetAppConfig("MP3Path", textBox1.Text.Trim());
            }
            MessageBox.Show("保存完毕");
            this.Close();
        }

        private void FormSet_Load(object sender, EventArgs e)
        {
            textBoxPicPath.Text = GetAppConfig("WatcherPath");//取得监视路径
            textBox1.Text = GetAppConfig("MP3Path");//取得MP3文件路径
            System.Drawing.Printing.PrintDocument print = new System.Drawing.Printing.PrintDocument();
            foreach (string sPrint in System.Drawing.Printing.PrinterSettings.InstalledPrinters)//获取所有打印机名称
            {

                comboBox1.Items.Add(sPrint);
                string sDefault = print.PrinterSettings.PrinterName;//默认打印机名
                if (sPrint == sDefault)
                    comboBox1.SelectedIndex = comboBox1.Items.IndexOf(sPrint);
            }
        }

        public  void SetAppConfig(string appKey, string appValue)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(System.Windows.Forms.Application.ExecutablePath + ".config");

            var xNode = xDoc.SelectSingleNode("//appSettings");

            var xElem = (XmlElement)xNode.SelectSingleNode("//add[@key='" + appKey + "']");
            if (xElem != null) xElem.SetAttribute("value", appValue);
            else
            {
                var xNewElem = xDoc.CreateElement("add");
                xNewElem.SetAttribute("key", appKey);
                xNewElem.SetAttribute("value", appValue);
                xNode.AppendChild(xNewElem);
            }
            xDoc.Save(System.Windows.Forms.Application.ExecutablePath + ".config");
        }

        public  string GetAppConfig(string appKey)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(System.Windows.Forms.Application.ExecutablePath + ".config");

            var xNode = xDoc.SelectSingleNode("//appSettings");

            var xElem = (XmlElement)xNode.SelectSingleNode("//add[@key='" + appKey + "']");

            if (xElem != null)
            {
                return xElem.Attributes["value"].Value;
            }
            return string.Empty;
        }


    }//formset end
}//命名空间 end
