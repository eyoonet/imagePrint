﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PrintImagProject
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FormReg reg = new FormReg();//构造时候验证过注册
            if (reg.IsRge == true)
            {
                Application.Run(new FormMain());
            }
            else
            {
                reg.ShowDialog();
                if (reg.IsRge == true)
                {
                    Application.Run(new FormMain());
                }
                else 
                {
                    Application.Exit();
                }
                
            }
        }
    }
}
