﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PrintImagProject
{
    public partial class FormReg : Form
    {
        RegisterClass reg = new RegisterClass();
        public bool IsRge=false;
        public FormReg()
        {
            string code = reg.CreateCode();
            IsRge = CheckIsReg(code, 
                CommonLib.XmlUtil.GetAppConfig(System.Windows.Forms.Application.ExecutablePath,"sn"));
            if (IsRge) { return; };
            InitializeComponent();
        }
        

        private void FormReg_Load(object sender, EventArgs e)
        {
            
            textBox1.Text = reg.CreateCode();
            //main.Show(this);
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (reg.GetCode(textBox1.Text) == textBox2.Text)
            {
                CommonLib.XmlUtil.SetAppConfig(System.Windows.Forms.Application.ExecutablePath,
                    "sn",
                    textBox2.Text);
                MessageBox.Show("注册成功!!", "成功", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Asterisk);
                IsRge = true;
                this.Close();
            }
            else 
            {
                MessageBox.Show("注册失败!!请联系供应商.." ,"错误", 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(textBox1.Text);
            MessageBox.Show("复制成功,请发送给供应商获取注册码!!", "成功",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
        }
        private bool CheckIsReg(string code,string regcode) 
        {
            return  reg.GetCode(code) == regcode ? true : false;
        }

    }
}
