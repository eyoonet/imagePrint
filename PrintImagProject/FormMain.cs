﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ImageLibrary;
using FileLibrary;
using PrintLibrary;
using MP3PlayerLibrary;
using System.Configuration;
using ThoughtWorks.QRCode.Codec;


namespace PrintImagProject
{
    public partial class FormMain : Form
    {
        private ImageProcessing image;
        private PrintImage print;
        private ImageConfig imConfig = new ImageConfig();
        private PrintConfig prConfig = new PrintConfig();
        private FileWatcher file = new FileWatcher(ConfigurationManager.AppSettings["WatcherPath"]);//监视路径
        private int number = 0 ;//记录打印数量
        private delegate void SetTextCallback(string text);//线程访问的委托声明
        private string blueIniPath;//千月配置文件路径
        private string blueUpPicPath;//读取蓝牙配置文件中的用户图片上传路径
        const int YES_MARGIN = 0;//有边框
        const int NO_MARGIN = 1;//无边框
        
        public FormMain()
        {
            InitializeComponent();
           
        }
        /// <summary>
        /// 线程传递设置打印数量
        /// </summary>
        /// <param name="text"></param>
        private void SetText(string text)
        {
            // InvokeRequired需要比较调用线程ID和创建线程ID
            // 如果它们不相同则返回true
            if (this.Msg_PrintStuts.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });//d, new object[] { text }
            }
            else
            {
                this.Msg_PrintStuts.Text ="已打印:"+ text;
            }
        }      
        /// <summary>
        /// 窗口加载
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_Load(object sender, EventArgs e)
        {
            //测试按钮
            button1.Visible = false;
            //加载配置
            imConfig = imConfig.Load();
            prConfig = prConfig.Load();
            //生成二维码
            QrCodeEncoderCreate();
            //tableLayoutPanel1.BackColor = Color.FromArgb(0, tableLayoutPanel1.BackColor);
            //Msg_Time.BackColor = Color.FromArgb(0, Msg_Time.BackColor);
            //注册事件
            imConfig.ImageSetEvent += MsgImageSet_Change;//注册设置尺寸事件 
            file.CreatedFileEventArgs += File_Created;   //注册文件创建事件 
            //UI界面 显示
            Msg_Image.Text = "打印模式：" + imConfig.Mode;
            button_裁剪.Text = imConfig.Layout == 0 ? "裁剪打印" : "居中打印";
            button_边框.Text = prConfig.PaperSource == YES_MARGIN ? "有边框" : "无边框";
            ServiceToolStripMenuItem.Text = file.Service ? "停止服务" : "开始服务";
            //蓝牙功能
            blueIniPath = AutoBlueLib.AutoBlue.GetBspsIniPath();//获取千月配置文件路径
            timer_Blue.Enabled = System.IO.File.Exists(blueIniPath) ? true : false;//如果没有千月配置就不开启蓝牙服务;
            blueUpPicPath = CommonLib.IniUtil.ReadIniData("OPPSERVER", "INBOXFOLDER", "", blueIniPath);//读取千月配置文件中的图片上传路径
        }
        
        /// <summary>
        /// 文件创建触发
        /// </summary>
        /// <param name="e"></param>
        private void File_Created(FileSystemEventArgs e)
        {
            //设置打印尺寸
            //string model = "6寸";
            //图片处理配置  写入到XML的
            //imconfig.SetImageSize(model);
            //imconfig.LoadData();//被上面写入到XML 这里就重新加载一下。为了可以支持PHP或者其他
            //打印配置 
            //prconfig.SetPrintPattern(model);
            //prconfig.LoadData();//同上面说明
            //prconfig.PrinterName = "EPSON R330 Series";//EPSON T50 Series EPSON R330 Series
            prConfig.DocumentName = e.Name;
            //图片处理 传入的是图片路径和图片配置类
            image = new ImageProcessing(e.FullPath, imConfig);
            image.Processing();
            image.Save(imConfig.OutPath);
            //预览图片
            pictureBox_预览.Image = image.Imaeg;
            //打印开始
            print = new PrintImage(prConfig,imConfig.OutPath);
            print.RunPrintImage();
            //打印后删除
            File.Delete(e.FullPath);
            number++;
            SetText(number.ToString());
        }

        #region 按钮事件
        private void pictureBox6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button_1寸_Click(object sender, EventArgs e)
        {
            imConfig.SetImageSize("1寸");
            prConfig.SetPrintPattern("1寸");
        }

        private void button_6寸_Click(object sender, EventArgs e)
        {
            imConfig.SetImageSize("6寸");
            prConfig.SetPrintPattern("6寸");
        }

        private void button_7寸_Click(object sender, EventArgs e)
        {
            imConfig.SetImageSize("7寸");
            prConfig.SetPrintPattern("7寸");
        }

        private void button_边框_Click(object sender, EventArgs e)
        {
            if (prConfig.PaperSource == YES_MARGIN)//有边框
            {
                prConfig.SetMargin("无边框");
                button_边框.Text = "无边框";
            }
            else 
            {
                prConfig.SetMargin("有边框");
                button_边框.Text = "有边框";                
            }
        }

        private void button_裁剪_Click(object sender, EventArgs e)
        {
            const int CENTER = 1;//居中
            const int CUT    = 0;//裁剪
            if (imConfig.Layout == CUT)
            {
                imConfig.Layout = CENTER;
                button_裁剪.Text = "居中打印";
            }
            else 
            {
                imConfig.Layout = CUT;
                button_裁剪.Text = "裁剪打印";
            }

        }

        private void button_2寸_Click(object sender, EventArgs e)
        {
            imConfig.SetImageSize("2寸"); 
            prConfig.SetPrintPattern("2寸");
        }

        private void button_A4_Click(object sender, EventArgs e)
        {
            imConfig.SetImageSize("A4");
            prConfig.SetPrintPattern("A4");
        }

        private void button_5寸_Click(object sender, EventArgs e)
        {
            imConfig.SetImageSize("5寸");
            prConfig.SetPrintPattern("5寸");
        }

        private void button_8寸_Click(object sender, EventArgs e)
        {
            imConfig.SetImageSize("8寸");
            prConfig.SetPrintPattern("8寸");
        }

        private void button_钱包_Click(object sender, EventArgs e)
        {
            imConfig.SetImageSize("钱包照片");
            prConfig.SetPrintPattern("钱包照片");
        }
        #endregion

        #region 菜单按钮
        /// <summary>
        /// 菜单 关于
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 关于ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 菜单 播放
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 播放MP3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MP3Player MP3 = new MP3Player(ConfigurationManager.AppSettings["MP3Path"], Handle);
            if (!MP3.PlayerStatus())
            {
                MP3.Open();
                MP3PlayerToolStripMenuItem.Text = "停止播放";
                MP3.ForPlayer();
            }
            else
            {
                MP3.CloseMedia();
                MP3PlayerToolStripMenuItem.Text = "开始播放";
            }
        }
        /// <summary>
        /// 菜单 服务
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ServiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!file.Service)
            {
                ServiceToolStripMenuItem.Text = "停止服务";
                file.Service = true;
            }
            else
            {
                ServiceToolStripMenuItem.Text = "开始服务";
                file.Service = false;
            }
        }
        /// <summary>
        /// 菜单 设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSet formset = new FormSet(ref prConfig ,ref file);//传递的是引用
            formset.Show(this);
        }
        #endregion
        /// <summary>
        /// UI时间显示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_msg_Tick(object sender, EventArgs e)
        {
            Msg_Time.Text =DateTime.Now.ToLongDateString() + DateTime.Now.ToLongTimeString();
        }

        /// <summary>
        /// 用户设置图片尺寸后执行 事件函数
        /// </summary>
        /// <param name="e"></param>
        private void MsgImageSet_Change(ImageConfig e) 
        {
            Msg_Image.Text = "打印模式："+ imConfig.Mode;
        }

        /// <summary>
        /// 代码测试
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
           // AutoBlueLib.AutoBlue.QyBlueAutoNext();
            
       
        }
        /// <summary>
        /// 生成二维码
        /// </summary>
        private void QrCodeEncoderCreate() 
        {
            string AddressIP = string.Empty;
            foreach (System.Net.IPAddress _IPAddress in System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList)
            {
                if (_IPAddress.AddressFamily.ToString() == "InterNetwork")
                {
                    AddressIP = _IPAddress.ToString();
                }
            }
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            pictureBox_条码.Image = qrCodeEncoder.Encode(@"http://"+AddressIP+":82"); 
        }
        /// <summary>
        /// 窗口关闭后执行保存设置
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            imConfig.Save();
            prConfig.Save();
        }
        /// <summary>
        /// 监视蓝牙对话
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Blue_Tick(object sender, EventArgs e)
        {
            AutoBlueLib.AutoBlue.QyBlueAutoNext();//自动点击按钮
            AutoBlueLib.AutoBlue.ForeachBluePicMove(blueUpPicPath, file.WatcherPath);//移动蓝牙上传的图片到监视路径
        }
    }
}
