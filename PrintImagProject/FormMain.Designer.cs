﻿namespace PrintImagProject
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.button_7寸 = new System.Windows.Forms.Button();
            this.button_8寸 = new System.Windows.Forms.Button();
            this.button_5寸 = new System.Windows.Forms.Button();
            this.button_钱包 = new System.Windows.Forms.Button();
            this.pictureBox_Top = new System.Windows.Forms.PictureBox();
            this.pictureBox_条码 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.button_2寸 = new System.Windows.Forms.Button();
            this.button_A4 = new System.Windows.Forms.Button();
            this.button_边框 = new System.Windows.Forms.Button();
            this.button_裁剪 = new System.Windows.Forms.Button();
            this.pictureBox_预览 = new System.Windows.Forms.PictureBox();
            this.button_1寸 = new System.Windows.Forms.Button();
            this.button_6寸 = new System.Windows.Forms.Button();
            this.FormMainMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MP3PlayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Msg_Time = new System.Windows.Forms.Label();
            this.panel_Bottom = new System.Windows.Forms.Panel();
            this.Msg_PrintStuts = new System.Windows.Forms.Label();
            this.Msg_Image = new System.Windows.Forms.Label();
            this.timer_msg = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.timer_Blue = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Top)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_条码)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_预览)).BeginInit();
            this.FormMainMenuStrip.SuspendLayout();
            this.panel_Bottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_7寸
            // 
            this.button_7寸.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(136)))), ((int)(((byte)(236)))));
            this.button_7寸.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_7寸.FlatAppearance.BorderSize = 0;
            this.button_7寸.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.button_7寸.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_7寸.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_7寸.Font = new System.Drawing.Font("黑体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_7寸.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_7寸.Location = new System.Drawing.Point(172, 402);
            this.button_7寸.Name = "button_7寸";
            this.button_7寸.Size = new System.Drawing.Size(125, 111);
            this.button_7寸.TabIndex = 3;
            this.button_7寸.Text = "7寸";
            this.button_7寸.UseVisualStyleBackColor = false;
            this.button_7寸.Click += new System.EventHandler(this.button_7寸_Click);
            // 
            // button_8寸
            // 
            this.button_8寸.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(173)))), ((int)(((byte)(91)))));
            this.button_8寸.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_8寸.FlatAppearance.BorderSize = 0;
            this.button_8寸.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.button_8寸.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_8寸.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_8寸.Font = new System.Drawing.Font("黑体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_8寸.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_8寸.Location = new System.Drawing.Point(303, 402);
            this.button_8寸.Name = "button_8寸";
            this.button_8寸.Size = new System.Drawing.Size(125, 111);
            this.button_8寸.TabIndex = 4;
            this.button_8寸.Text = "8寸";
            this.button_8寸.UseVisualStyleBackColor = false;
            this.button_8寸.Click += new System.EventHandler(this.button_8寸_Click);
            // 
            // button_5寸
            // 
            this.button_5寸.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(64)))), ((int)(((byte)(12)))));
            this.button_5寸.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_5寸.FlatAppearance.BorderSize = 0;
            this.button_5寸.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.button_5寸.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_5寸.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_5寸.Font = new System.Drawing.Font("黑体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_5寸.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_5寸.Location = new System.Drawing.Point(434, 317);
            this.button_5寸.Name = "button_5寸";
            this.button_5寸.Size = new System.Drawing.Size(125, 80);
            this.button_5寸.TabIndex = 5;
            this.button_5寸.Text = "5寸";
            this.button_5寸.UseVisualStyleBackColor = false;
            this.button_5寸.Click += new System.EventHandler(this.button_5寸_Click);
            // 
            // button_钱包
            // 
            this.button_钱包.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(39)))), ((int)(((byte)(137)))));
            this.button_钱包.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_钱包.FlatAppearance.BorderSize = 0;
            this.button_钱包.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.button_钱包.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_钱包.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_钱包.Font = new System.Drawing.Font("黑体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_钱包.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_钱包.Location = new System.Drawing.Point(434, 402);
            this.button_钱包.Name = "button_钱包";
            this.button_钱包.Size = new System.Drawing.Size(125, 111);
            this.button_钱包.TabIndex = 6;
            this.button_钱包.Text = "钱包照片";
            this.button_钱包.UseVisualStyleBackColor = false;
            this.button_钱包.Click += new System.EventHandler(this.button_钱包_Click);
            // 
            // pictureBox_Top
            // 
            this.pictureBox_Top.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(31)))), ((int)(((byte)(74)))));
            this.pictureBox_Top.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox_Top.Name = "pictureBox_Top";
            this.pictureBox_Top.Size = new System.Drawing.Size(786, 40);
            this.pictureBox_Top.TabIndex = 10;
            this.pictureBox_Top.TabStop = false;
            // 
            // pictureBox_条码
            // 
            this.pictureBox_条码.Image = global::PrintImagProject.Properties.Resources.Code;
            this.pictureBox_条码.Location = new System.Drawing.Point(565, 340);
            this.pictureBox_条码.Name = "pictureBox_条码";
            this.pictureBox_条码.Size = new System.Drawing.Size(173, 173);
            this.pictureBox_条码.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_条码.TabIndex = 13;
            this.pictureBox_条码.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(670, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(60, 20);
            this.pictureBox5.TabIndex = 14;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(730, 0);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(56, 20);
            this.pictureBox6.TabIndex = 15;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Visible = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // button_2寸
            // 
            this.button_2寸.BackColor = System.Drawing.Color.Fuchsia;
            this.button_2寸.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button_2寸.FlatAppearance.BorderSize = 0;
            this.button_2寸.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_2寸.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_2寸.Font = new System.Drawing.Font("黑体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_2寸.ForeColor = System.Drawing.SystemColors.Control;
            this.button_2寸.Location = new System.Drawing.Point(434, 142);
            this.button_2寸.Name = "button_2寸";
            this.button_2寸.Size = new System.Drawing.Size(125, 74);
            this.button_2寸.TabIndex = 1;
            this.button_2寸.Text = "2寸";
            this.button_2寸.UseVisualStyleBackColor = false;
            this.button_2寸.Click += new System.EventHandler(this.button_2寸_Click);
            // 
            // button_A4
            // 
            this.button_A4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(129)))), ((int)(((byte)(129)))));
            this.button_A4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_A4.FlatAppearance.BorderSize = 0;
            this.button_A4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.button_A4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_A4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_A4.Font = new System.Drawing.Font("黑体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_A4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_A4.Location = new System.Drawing.Point(434, 222);
            this.button_A4.Name = "button_A4";
            this.button_A4.Size = new System.Drawing.Size(125, 89);
            this.button_A4.TabIndex = 7;
            this.button_A4.Text = "A4";
            this.button_A4.UseVisualStyleBackColor = false;
            this.button_A4.Click += new System.EventHandler(this.button_A4_Click);
            // 
            // button_边框
            // 
            this.button_边框.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(199)))), ((int)(((byte)(199)))));
            this.button_边框.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_边框.FlatAppearance.BorderSize = 0;
            this.button_边框.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.button_边框.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_边框.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_边框.Font = new System.Drawing.Font("黑体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_边框.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_边框.Location = new System.Drawing.Point(565, 172);
            this.button_边框.Name = "button_边框";
            this.button_边框.Size = new System.Drawing.Size(173, 162);
            this.button_边框.TabIndex = 8;
            this.button_边框.Text = "有边框";
            this.button_边框.UseVisualStyleBackColor = false;
            this.button_边框.Click += new System.EventHandler(this.button_边框_Click);
            // 
            // button_裁剪
            // 
            this.button_裁剪.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(103)))), ((int)(((byte)(187)))));
            this.button_裁剪.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_裁剪.FlatAppearance.BorderSize = 0;
            this.button_裁剪.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.button_裁剪.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_裁剪.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_裁剪.Font = new System.Drawing.Font("黑体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_裁剪.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_裁剪.Location = new System.Drawing.Point(565, 57);
            this.button_裁剪.Name = "button_裁剪";
            this.button_裁剪.Size = new System.Drawing.Size(173, 109);
            this.button_裁剪.TabIndex = 9;
            this.button_裁剪.Text = "裁剪打印";
            this.button_裁剪.UseVisualStyleBackColor = false;
            this.button_裁剪.Click += new System.EventHandler(this.button_裁剪_Click);
            // 
            // pictureBox_预览
            // 
            this.pictureBox_预览.BackgroundImage = global::PrintImagProject.Properties.Resources.PhotoBackgroundImage;
            this.pictureBox_预览.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox_预览.Location = new System.Drawing.Point(41, 57);
            this.pictureBox_预览.Name = "pictureBox_预览";
            this.pictureBox_预览.Size = new System.Drawing.Size(387, 339);
            this.pictureBox_预览.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_预览.TabIndex = 12;
            this.pictureBox_预览.TabStop = false;
            // 
            // button_1寸
            // 
            this.button_1寸.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_1寸.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_1寸.FlatAppearance.BorderSize = 0;
            this.button_1寸.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.button_1寸.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_1寸.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_1寸.Font = new System.Drawing.Font("黑体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_1寸.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_1寸.Location = new System.Drawing.Point(434, 57);
            this.button_1寸.Name = "button_1寸";
            this.button_1寸.Size = new System.Drawing.Size(125, 79);
            this.button_1寸.TabIndex = 17;
            this.button_1寸.Text = "1寸";
            this.button_1寸.UseVisualStyleBackColor = false;
            this.button_1寸.Click += new System.EventHandler(this.button_1寸_Click);
            // 
            // button_6寸
            // 
            this.button_6寸.BackColor = System.Drawing.Color.Red;
            this.button_6寸.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button_6寸.FlatAppearance.BorderSize = 0;
            this.button_6寸.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Navy;
            this.button_6寸.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button_6寸.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_6寸.Font = new System.Drawing.Font("黑体", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_6寸.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button_6寸.Location = new System.Drawing.Point(41, 402);
            this.button_6寸.Name = "button_6寸";
            this.button_6寸.Size = new System.Drawing.Size(125, 111);
            this.button_6寸.TabIndex = 18;
            this.button_6寸.Text = "6寸";
            this.button_6寸.UseVisualStyleBackColor = false;
            this.button_6寸.Click += new System.EventHandler(this.button_6寸_Click);
            // 
            // FormMainMenuStrip
            // 
            this.FormMainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ServiceToolStripMenuItem,
            this.MP3PlayerToolStripMenuItem,
            this.设置ToolStripMenuItem,
            this.关于ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.FormMainMenuStrip.Name = "contextMenuStrip1";
            this.FormMainMenuStrip.Size = new System.Drawing.Size(125, 114);
            // 
            // ServiceToolStripMenuItem
            // 
            this.ServiceToolStripMenuItem.Name = "ServiceToolStripMenuItem";
            this.ServiceToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.ServiceToolStripMenuItem.Text = "开始服务";
            this.ServiceToolStripMenuItem.Click += new System.EventHandler(this.ServiceToolStripMenuItem_Click);
            // 
            // MP3PlayerToolStripMenuItem
            // 
            this.MP3PlayerToolStripMenuItem.Name = "MP3PlayerToolStripMenuItem";
            this.MP3PlayerToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.MP3PlayerToolStripMenuItem.Text = "开始播放";
            this.MP3PlayerToolStripMenuItem.Click += new System.EventHandler(this.播放MP3ToolStripMenuItem_Click);
            // 
            // 设置ToolStripMenuItem
            // 
            this.设置ToolStripMenuItem.Name = "设置ToolStripMenuItem";
            this.设置ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.设置ToolStripMenuItem.Text = "设置";
            this.设置ToolStripMenuItem.Click += new System.EventHandler(this.设置ToolStripMenuItem_Click);
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.关于ToolStripMenuItem.Text = "关于";
            this.关于ToolStripMenuItem.Click += new System.EventHandler(this.关于ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            // 
            // Msg_Time
            // 
            this.Msg_Time.AutoSize = true;
            this.Msg_Time.BackColor = System.Drawing.Color.Transparent;
            this.Msg_Time.Cursor = System.Windows.Forms.Cursors.Default;
            this.Msg_Time.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Msg_Time.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Msg_Time.Location = new System.Drawing.Point(11, 11);
            this.Msg_Time.Name = "Msg_Time";
            this.Msg_Time.Size = new System.Drawing.Size(111, 16);
            this.Msg_Time.TabIndex = 19;
            this.Msg_Time.Text = "时间初始化..";
            // 
            // panel_Bottom
            // 
            this.panel_Bottom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(31)))), ((int)(((byte)(74)))));
            this.panel_Bottom.Controls.Add(this.Msg_PrintStuts);
            this.panel_Bottom.Controls.Add(this.Msg_Image);
            this.panel_Bottom.Controls.Add(this.Msg_Time);
            this.panel_Bottom.Location = new System.Drawing.Point(-1, 528);
            this.panel_Bottom.Name = "panel_Bottom";
            this.panel_Bottom.Size = new System.Drawing.Size(785, 33);
            this.panel_Bottom.TabIndex = 20;
            // 
            // Msg_PrintStuts
            // 
            this.Msg_PrintStuts.AutoSize = true;
            this.Msg_PrintStuts.BackColor = System.Drawing.Color.Transparent;
            this.Msg_PrintStuts.Cursor = System.Windows.Forms.Cursors.Default;
            this.Msg_PrintStuts.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Msg_PrintStuts.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Msg_PrintStuts.Location = new System.Drawing.Point(542, 11);
            this.Msg_PrintStuts.Name = "Msg_PrintStuts";
            this.Msg_PrintStuts.Size = new System.Drawing.Size(76, 16);
            this.Msg_PrintStuts.TabIndex = 21;
            this.Msg_PrintStuts.Text = "已打印：";
            // 
            // Msg_Image
            // 
            this.Msg_Image.AutoSize = true;
            this.Msg_Image.BackColor = System.Drawing.Color.Transparent;
            this.Msg_Image.Cursor = System.Windows.Forms.Cursors.Default;
            this.Msg_Image.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Msg_Image.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Msg_Image.Location = new System.Drawing.Point(333, 9);
            this.Msg_Image.Name = "Msg_Image";
            this.Msg_Image.Size = new System.Drawing.Size(93, 16);
            this.Msg_Image.TabIndex = 20;
            this.Msg_Image.Text = "打印模式：";
            // 
            // timer_msg
            // 
            this.timer_msg.Enabled = true;
            this.timer_msg.Interval = 1000;
            this.timer_msg.Tick += new System.EventHandler(this.timer_msg_Tick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(-1, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer_Blue
            // 
            this.timer_Blue.Interval = 5000;
            this.timer_Blue.Tick += new System.EventHandler(this.timer_Blue_Tick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.ContextMenuStrip = this.FormMainMenuStrip;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel_Bottom);
            this.Controls.Add(this.button_钱包);
            this.Controls.Add(this.button_6寸);
            this.Controls.Add(this.button_5寸);
            this.Controls.Add(this.button_8寸);
            this.Controls.Add(this.button_裁剪);
            this.Controls.Add(this.button_7寸);
            this.Controls.Add(this.button_2寸);
            this.Controls.Add(this.button_A4);
            this.Controls.Add(this.button_边框);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.button_1寸);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox_Top);
            this.Controls.Add(this.pictureBox_预览);
            this.Controls.Add(this.pictureBox_条码);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Top)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_条码)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_预览)).EndInit();
            this.FormMainMenuStrip.ResumeLayout(false);
            this.panel_Bottom.ResumeLayout(false);
            this.panel_Bottom.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_7寸;
        private System.Windows.Forms.Button button_8寸;
        private System.Windows.Forms.Button button_5寸;
        private System.Windows.Forms.Button button_钱包;
        private System.Windows.Forms.PictureBox pictureBox_Top;
        private System.Windows.Forms.PictureBox pictureBox_条码;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button button_2寸;
        private System.Windows.Forms.Button button_A4;
        private System.Windows.Forms.Button button_边框;
        private System.Windows.Forms.Button button_裁剪;
        private System.Windows.Forms.PictureBox pictureBox_预览;
        private System.Windows.Forms.Button button_1寸;
        private System.Windows.Forms.Button button_6寸;
        private System.Windows.Forms.ContextMenuStrip FormMainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.Label Msg_Time;
        private System.Windows.Forms.Panel panel_Bottom;
        private System.Windows.Forms.Timer timer_msg;
        private System.Windows.Forms.Label Msg_Image;
        private System.Windows.Forms.Label Msg_PrintStuts;
        private System.Windows.Forms.ToolStripMenuItem MP3PlayerToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer_Blue;

    }
}

