﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
namespace MP3PlayerLibrary
{
    /// <summary>
    /// private void Main()
    ///{
 　 ///　MP3Player MP3 = new MP3Player (Properties.Resources.music, Handle);
    ///  mp3.Open ();
    ///}
    ///
    /// 12.获取播放当前状态：
    /// mciSendString "status movie mode", ST, Len(ST), 0
    /// If Left(ST, 7) = "stopped" Then (处理代码) '播放完毕
    /// 13.循环播放：
    /// mciSendString "play movie repeat", 0&, 0, 0
    /// </summary>
      public class MP3Player
      {
        [DllImport ("winmm.dll")]
        static extern Int32 mciSendString (String command,StringBuilder buffer, Int32 bufferSize, IntPtr hwndCallback);
        /// <summary>
        /// temporary repository of music files
        /// </ summary>
        private String m_musicPath = "";
        /// <summary>
        /// parent window handle
        /// </ summary>
        private IntPtr m_Handle;

        public bool PlayerStatus() 
        {
            StringBuilder st=new StringBuilder();
            st.Length = 16;
            mciSendString("status Media mode", st, st.Length, IntPtr.Zero);
            st.Length = 7;
            return st.ToString() == "playing" ? true : false;
            //mciSendString("play Media repeat", st, st.Length, IntPtr.Zero);
        }


        public void ForPlayer() 
        {
            StringBuilder st = new StringBuilder();
            mciSendString("play Media repeat", st, st.Length, IntPtr.Zero);
        }

        /// <summary>
        /// Create Mp3 player class
        /// </ summary>
        /// <PARAM name="music">embedded music file</ param>
        /// <PARAM name="path">temporary music file path</ param>
        /// <PARAM name="Handle">parent window handle</ param>
        public MP3Player (Byte [] Music, String path, IntPtr Handle)
        {
          try
          {
            m_Handle = Handle;
            m_musicPath = Path.Combine (path, "temp.mp3");
            FileStream fs = new FileStream (m_musicPath, FileMode.Create);
            fs.Write (Music, 0, Music.Length);
            fs.Close ();
          }
          catch (Exception)
          {
          }
        }
    
        /// <summary>
        /// Create Mp3 player class
        /// </ summary>
        /// <PARAM name="musicPath">to play the mp3 file path</ param>
        /// <PARAM name="Handle">parent window handle</ param>
        public MP3Player(String musicPath, IntPtr Handle)
        {
          m_musicPath = musicPath;
          m_Handle = Handle;
        }
    
        public MP3Player(Byte [] Music, IntPtr Handle) : this(Music, @"C:\Windows\",Handle)
        {
        }
    
    
        public void Open (String path)
        {
          if (path != "")
          {
            try
            {
              mciSendString ("Open " + path + " alias Media", null, 0, m_Handle);
              mciSendString ("play Media", null, 0, m_Handle);
            }
            catch (Exception)
            {
            }
          }
        }
    
    
        public void Open()
        {
          Open (m_musicPath);
        }
   
      
        public void CloseMedia()
        {
          try
          {
            mciSendString ("Close ALL", null, 0, m_Handle);
          }
          catch (Exception)
          {
          }
        }
  }
}
