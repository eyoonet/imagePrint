﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Printing;
using System.Drawing;

namespace PrintLibrary
{
    public class PrintImage
    {
        #region 字段信息
        //打印设置
        private PrintConfig config;
        public PrintConfig Config 
        { 
            get { return config; } 
            set { config = value; } 
        }
        //声明一个打印文档
        private PrintDocument pd;
        //打印图片的路径
        private string path; 
        public string Path 
        { 
            get { return path; }
            set { path = value; }
        }
        //图片资源
        private Image image;
        public Image Image 
        { 
            get { return image; } 
            set { image = value; }
        }
        //声明委托传入打印数量 列队数量 
        public delegate void PrintEventHandler(PrintImage e);
        public event PrintEventHandler PrintEventArgs; //声明事件
        
        #endregion

        #region 事件方法
        protected virtual void OnPrint(PrintImage e)
        {
            if (PrintEventArgs != null)
            { // 如果有对象注册
                PrintEventArgs(e);  // 调用所有注册对象的方法
            }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="path">路径</param>
        public PrintImage(PrintConfig config,string path) 
        {
           this.config = config;
           this.path = path;
           
            try
           {
              Image img = System.Drawing.Image.FromFile(path);
              this.image = new System.Drawing.Bitmap(img);
              img.Dispose();   
            }
            catch(Exception e) {
                throw e;
            }
            // System.Windows.Forms.MessageBox.Show(rotate.ToString());
        }
        #endregion

        #region 打印页面设置
        /// <summary>
        /// 传入打印页的函数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev">打印页的设置信息</param>
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            //	DrawImage(Image, Rectangle, Rectangle, GraphicsUnit)
            //ev.Graphics.PageUnit = GraphicsUnit.Millimeter;
            //画图 原图 原大小
            ev.Graphics.DrawImage(this.image, 0, 0);
            //Rectangle destRect = new Rectangle(0, 0, 400, 600);
            //Rectangle srcRect = new Rectangle(0, 0, this.image.Width, this.image.Height);
           // GraphicsUnit units = GraphicsUnit.Pixel;
           // ev.Graphics.DrawImage(this.image, destRect, srcRect, units);
        }
        #endregion

        #region 打印机设置
        public void RunPrintImage() 
        {            
            //打印机实例化
            this.pd = new PrintDocument();
            //取消打印状态对话框
            this.pd.PrintController = new StandardPrintController();
            //打印文档的名称
            this.pd.DocumentName = this.config.DocumentName;
            //指定打印机打印
            this.pd.PrinterSettings.PrinterName = this.config.PrinterName;
            //pd.PrinterSettings.PaperSources[1];无边距纸盒的信息
            pd.DefaultPageSettings.PaperSource = pd.PrinterSettings.PaperSources[this.config.PaperSource];//无边距需要纸盒的支持
            //打印纸设置
            //PaperSize diyPaperSize = new PaperSize("diy",400,600);
            //pd.DefaultPageSettings.PaperSize = diyPaperSize;
            pd.DefaultPageSettings.PaperSize =  pd.PrinterSettings.PaperSizes[this.config.PaperSize];
            //打印方向设置true横向fales纵向
            pd.DefaultPageSettings.Landscape = image.Width > image.Height ? true : false;
            //打印分辨率
            //PrinterResolution {[PrinterResolution X=720 Y=720]}	System.Drawing.Printing.PrinterResolution
            //绑定打印事件
            this.pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
            //开始打印
            pd.Print();
            OnPrint(this);//触发打印事件
        }
        #endregion
    }

}
