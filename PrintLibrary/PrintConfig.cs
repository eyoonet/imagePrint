﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
namespace PrintLibrary
{
  
     public class PrintConfig 
    {
        #region 字段信息
        public const string FILE_NAME = "PrintImageConfig.config";
        private XmlDocument xml = new XmlDocument();
        //打印模式名
        private string printmode;
        public string Printmode
        {
            get { return printmode; }
            set { printmode = value; }
        }
        //打印文档名
        private string documentname;
        public string DocumentName 
        {   get { return documentname; } 
            set { documentname = value; } 
        }
        //打印机名
        private string printername;
        public string PrinterName 
        { 
            get { return printername; }
            set { printername = value; }
        }
        //送纸纸盒
        private int papersource;
        public int PaperSource 
        { 
            get { return papersource; } 
            set { papersource = value; }
        }
        //打印纸设置
        private int papersize;
        public int PaperSize 
        { 
            get { return papersize; } 
            set { papersize = value; } 
        }
        //纸大小列表
        public List<PrintConfig> Configs = new List<PrintConfig>();  
        #endregion
        
        public  PrintConfig(){}
        public PrintConfig(string mode, int papersize)
        {
            this.printmode = mode; this.papersize = papersize; 
        }

        public PrintConfig GetPrintPattern(string Pattern)
        {
            foreach(PrintConfig cof in Configs)
            {
                if (cof.printmode == Pattern)
               {
                   cof.PrinterName = printername;
                   cof.DocumentName = documentname;
                   return cof;
               }
            }
            return null;
        }
        /// <summary>
        /// 设置打印尺寸通过modename
        /// </summary>
        /// <param name="Pattern"></param>
        public void SetPrintPattern(string Pattern)
        {
            foreach (PrintConfig cof in Configs)
            {
                if (cof.printmode == Pattern)
                {
                    printmode = cof.Printmode;
                    papersource = cof.PaperSource;
                    papersize = cof.PaperSize;
                    //this.Save();
                    return;
                }
            }
        }
        /// <summary>
        /// 设置打印边距
        /// </summary>
        /// <param name="Margin"></param>
        public void SetMargin(string Margin)
        {
            if(Margin=="有边框")
            {
                papersource = 0;
            }
            if(Margin=="无边框")
            {
                papersource = 1;
            }
        }
        /// <summary>
        /// 序列化保存
        /// </summary>
        public void Save() 
        {
            string xmlstr = XmlUtil.Serializer(typeof(PrintConfig), this);
            xml.LoadXml(xmlstr);
            xml.Save(FILE_NAME);
        }
        /// <summary>
        /// 序列化加载
        /// </summary>
        /// <returns>返回配置</returns>
        public PrintConfig  Load() 
        {
            xml.Load(FILE_NAME);
            PrintConfig stu2 = XmlUtil.Deserialize(typeof(PrintConfig), xml.InnerXml) as PrintConfig;
            return stu2;
        }

    }


     public class XmlUtil
     {
         #region 反序列化
         /// <summary>
         /// 反序列化
         /// </summary>
         /// <param name="type">类型</param>
         /// <param name="xml">XML字符串</param>
         /// <returns></returns>
         public static object Deserialize(Type type, string xml)
         {
             try
             {
                 using (StringReader sr = new System.IO.StringReader(xml))
                 {
                     System.Xml.Serialization.XmlSerializer xmldes = new System.Xml.Serialization.XmlSerializer(type);
                     return xmldes.Deserialize(sr);
                 }
             }
             catch (Exception)
             {

                 return null;
             }
         }
         /// <summary>
         /// 反序列化
         /// </summary>
         /// <param name="type"></param>
         /// <param name="xml"></param>
         /// <returns></returns>
         public static object Deserialize(Type type, Stream stream)
         {
             System.Xml.Serialization.XmlSerializer xmldes = new System.Xml.Serialization.XmlSerializer(type);
             return xmldes.Deserialize(stream);
         }
         #endregion

         #region 序列化
         /// <summary>
         /// 序列化
         /// </summary>
         /// <param name="type">类型</param>
         /// <param name="obj">对象</param>
         /// <returns></returns>
         public static string Serializer(Type type, object obj)
         {
             MemoryStream Stream = new MemoryStream();
             System.Xml.Serialization.XmlSerializer xml = new System.Xml.Serialization.XmlSerializer(type);
             try
             {
                 //序列化对象
                 xml.Serialize(Stream, obj);
             }
             catch (InvalidOperationException)
             {
                 throw;
             }
             Stream.Position = 0;
             StreamReader sr = new StreamReader(Stream);
             string str = sr.ReadToEnd();

             sr.Dispose();
             Stream.Dispose();

             return str;
         }

         #endregion
     }
    
    
    /*
    public class PrintConfig
    {
        #region 字段信息
        private const string FILE_NAME = "PrintImageConfig.config";
        private XmlDocument xml = new XmlDocument(); 
        //打印文档名
        private string documentname;
        public  string DocumentName{get{return documentname;} set{documentname=value;} }
        //打印机名
        private string printername;
        public  string PrinterName{ get {return printername;} set{printername = value;} }
        //送纸纸盒
        private int papersource;
        public int PaperSource { get { return papersource; } set { papersource = value; } }
        //打印纸设置
        private int papersize;
        public int PaperSize { get { return papersize; } set { papersize = value; } }
        //打印方向  可在打印构造时候设置
        //private bool Landscape;
        #endregion

        #region 打印设置
        /// <summary>
        /// 设置打印模式
        /// 使用方法：clss.SetPrintPattern("6寸","有边框");
        /// </summary>
        /// <param name="Pattern">打印尺寸</param>
        /// <param name="Margin">边框</param>
        public void SetPrintPattern(string Pattern)
        {
            //XmlNode PaperSizes = this.xml.SelectSingleNode("PrintImageConfig/PaperSizes");
            XmlNodeList nodeList = this.xml.SelectSingleNode("PrintImageConfig/PaperSizes").ChildNodes;//获取PaperSizes节点的所有子节点 
            foreach (XmlNode xn in nodeList)//遍历所有子节点 
            {
                XmlElement xe = (XmlElement)xn;//将子节点类型转换为XmlElement类型 
                if(xe.InnerText == Pattern)
                {
                    this.papersize =Int32.Parse(xe.GetAttribute("id"));
                    //保存到配置文件
                    //this.Save();
                    break;
                }
            }
            this.Save();
        }

        public void SetMargin(string Margin) 
        {
            //string Margin = MarginBool ? "无边框" : "有边框";
            XmlNodeList nodeList2 = this.xml.SelectSingleNode("PrintImageConfig/PaperSources").ChildNodes;//获取PaperSizes节点的所有子节点 
            foreach (XmlNode xn in nodeList2)//遍历所有子节点 
            {
                XmlElement xe = (XmlElement)xn;//将子节点类型转换为XmlElement类型 
                if (xe.InnerText == Margin)
                {
                    this.papersource = Int32.Parse(xe.GetAttribute("id"));
                    //保存到配置文件
                    //this.Save();
                    break;
                }
            }
            this.Save();
        }
        #endregion

        #region 加载数据
        /// <summary>
        /// 加载xml文件配置
        /// </summary>
        public void LoadData() {
            //打印文档名
            XmlNode DocumentName = this.xml.SelectSingleNode("PrintImageConfig/DocumentName");
            this.documentname = DocumentName.InnerText;
            //打印机名
            XmlNode PrinterNmame = this.xml.SelectSingleNode("PrintImageConfig/PrinterNmame");
            this.printername = PrinterNmame.InnerText;
            //送纸纸盒
            XmlNode PaperSource = this.xml.SelectSingleNode("PrintImageConfig/PaperSource");
            int.TryParse(PaperSource.InnerText, out this.papersource);
            //纸张大小
            XmlNode PaperSize = this.xml.SelectSingleNode("PrintImageConfig/PaperSize");
            int.TryParse(PaperSize.InnerText, out this.papersize);
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public PrintConfig() {
            try
            {
                xml.Load(FILE_NAME);
                this.LoadData();
            }
            catch (DirectoryNotFoundException)
            {
                throw new DirectoryNotFoundException("打印机配置文件不存在");
            }

        }
        #endregion

        #region 保存数据
        /// <summary>
        /// 保存配置文件
        /// </summary>
        public void Save() 
        {
            //打印文档名
            XmlNode DocumentName = this.xml.SelectSingleNode("PrintImageConfig/DocumentName");
            DocumentName.InnerText = this.documentname;
            //打印机名
            XmlNode PrinterNmame = this.xml.SelectSingleNode("PrintImageConfig/PrinterNmame");
            PrinterNmame.InnerText = this.printername;
            //送纸纸盒
            XmlNode PaperSource = this.xml.SelectSingleNode("PrintImageConfig/PaperSource");
            PaperSource.InnerText = this.papersource.ToString();
            
            //纸张大小
            XmlNode PaperSize = this.xml.SelectSingleNode("PrintImageConfig/PaperSize");
            PaperSize.InnerText = this.papersize.ToString();
            this.xml.Save(FILE_NAME);
        }
        #endregion
    }
   */
}
