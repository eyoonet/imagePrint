﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace AutoBlueLib
{
    //C:\Users\Administrator\AppData\Local\bluesoleil\bsps.ini 是千月蓝牙的配置文件可设置路径
    //string temp = System.Environment.GetEnvironmentVariable("TEMP");  LOCALAPPDATA=C:\Users\ user\AppData\Local
    //DirectoryInfo info = new DirectoryInfo(temp);  
    public class AutoBlue
    {
        [DllImport("user32.dll")]
        public static extern int PostMessageA(IntPtr hwnd, int wMsg, int wParam, int lParam);

        //[DllImport("user32.dll")]
        //public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll", EntryPoint = "GetWindowText")]//获取标题
        public static extern int GetWindowText(IntPtr hwnd, StringBuilder lpString, int cch);

        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "FindWindowEx", SetLastError = true)]
        private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        //[DllImport("User32.dll", EntryPoint = "SendMessage")]
        //private static extern int SendMessage(IntPtr hWnd,int Msg, IntPtr wParam, string lParam);
        //private static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

        //const int WM_GETTEXT = 0x000D;
        //const int WM_SETTEXT = 0x000C;
        //const int WM_CLICK = 0x00F5;
        /// <summary>
        /// 获取千月窗口句柄 蓝牙口令
        /// </summary>
        private static IntPtr GetBlueHwnd(string title)
        {
            IntPtr ParenthWnd = new IntPtr(0);

            ParenthWnd = FindWindow("#32770", null);
            if (!ParenthWnd.Equals(IntPtr.Zero))
            {
                StringBuilder s = new StringBuilder(512);
                int i = GetWindowText(ParenthWnd, s, s.Capacity);
                s.Length = 4;
                string a = s.ToString();
                if (s.ToString() != title)
                {
                    ParenthWnd = IntPtr.Zero;
                }
            }
            return ParenthWnd;
        }

        /// <summary>
        /// 查找按钮并单击
        /// </summary>
        /// <param name="parentHwnd"></param>
        /// <param name="lpszName_Submit"></param>
        private static void Click(IntPtr parentHwnd, string lpszName_Submit)
        {
            IntPtr EdithWnd = new IntPtr(0);
            EdithWnd = FindWindowEx(parentHwnd, (IntPtr)0, "Button", lpszName_Submit);
            if (!EdithWnd.Equals(IntPtr.Zero))
            {
                //SendMessage(EdithWnd, WM_CLICK, 0, 0);
                //System.Threading.Thread.Sleep(5000);
                //SetForegroundWindow(parentHwnd);
                //System.Threading.Thread.Sleep(3000);
                //SendMessage(parentHwnd, WM_CLICK, (IntPtr)0, (IntPtr)0);
                PostMessageA(EdithWnd, 256, 13, 1835009);
                PostMessageA(EdithWnd, 257, 13, 1835009);
            }
        }
        /// <summary>
        /// 千月蓝牙窗口处理
        /// </summary>
        public static void QyBlueAutoNext()
        {
            IntPtr hwnd = GetBlueHwnd("蓝牙口令");
            if (!hwnd.Equals(IntPtr.Zero))
            {
                Click(hwnd, "确定(&O)");
            }
            hwnd = GetBlueHwnd("蓝牙服务");
            if (!hwnd.Equals(IntPtr.Zero))
            {
                Click(hwnd, "是(&Y)");
            }
            //ForeachBluePicMove(@"C:\Users\Administrator\Documents\Bluetooth\inbox", @"C:\Users\Administrator\Desktop\新建文件夹\")
            ///System.IO.DirectoryInfo TheFolder = new System.IO.DirectoryInfo(@"path");
            //遍历文件夹
            /// foreach (System.IO.DirectoryInfo NextFolder in TheFolder.GetDirectories())
                ;// this.listBox1.Items.Add(NextFolder.Name);
            //遍历文件
            /// foreach (System.IO.FileInfo NextFile in TheFolder.GetFiles())
                ; //  this.listBox2.Items.Add(NextFile.Name);
            //移动文件
            /// System.IO.File.Move("c:/a/xiaoming.jpg", "e:/a/xiaoming.jpg");
                //System.IO.File.Exists("文件是否存在");
        }
        public static void ForeachBluePicMove(string adPath,string dsPath) 
        {
            //List<string> s = new List<string>();
            if (!System.IO.Directory.Exists(adPath)) return;//不存在文件夹返回
            System.IO.DirectoryInfo TheFolder = new System.IO.DirectoryInfo(adPath);
            foreach (System.IO.FileInfo NextFile in TheFolder.GetFiles()) //NextFile.FullName
                try
                {
                    System.IO.File.Move(NextFile.FullName, dsPath + @"/" + NextFile.Name);
                }catch(System.IO.IOException e)
                {
                    System.IO.File.Delete(dsPath + @"/" + NextFile.Name);
                } 
        }
        
        
        /// <summary>
        /// 获取千月配置文件路径
        /// </summary>
        /// <returns></returns>
        public static string GetBspsIniPath()
        {
            string temp = System.Environment.GetEnvironmentVariable("LOCALAPPDATA") + @"\bluesoleil\bsps.ini";  //LOCALAPPDATA=C:\Users\ user\AppData\Local
            return temp;
            //System.IO.DirectoryInfo info = new System.IO.DirectoryInfo(temp);  
        }
    }
}
