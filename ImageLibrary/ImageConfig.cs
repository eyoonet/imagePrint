﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
namespace ImageLibrary
{
    public class ImageConfig
    {
        #region 字段信息
        private const string FILE_NAME = @"ImageConfig.config";
        private XmlDocument xml = new XmlDocument();
        //打印模式名
        private string mode;
        public string Mode
        {
            get { return mode; }
            set { mode = value; }
        }
        //图片宽度
        private int width;
        public int Width
        {
            get { return width; }
            set { width = value; }
        }
        //图片高度
        private int heigth;
        public int Heigth
        {
            get { return heigth; }
            set { heigth = value; }
        }
        //类型  存在1寸循环铺盖所以加了类型来判断
        private int type;
        public int Type
        {
            get { return type; }
            set { type = value; }
        }
        //输出路径
        private string outpath;
        public string OutPath
        {
            get { return outpath; }
            set { outpath = value; }
        }
        //布局  居中或裁剪 0 是裁剪 1 是居中
        private int layout = 0;
        public int Layout
        {
            get { return layout; }
            set { layout = value; }
        }
        public List<ImageConfig> Configs = new List<ImageConfig>();

        //事件委托
        public delegate void ImageSetEventHandler(ImageConfig e);
        public event ImageSetEventHandler ImageSetEvent; //声明事件        
        #endregion

        public ImageConfig() { }
        public ImageConfig(string mode, int width, int height, int type)
        {
            this.mode = mode; this.width = width; this.heigth = height; this.type = type;
        }

        public void SetImageSize(string Pattern)
        {
            foreach (ImageConfig cof in Configs)
            {
                if (cof.mode == Pattern)
                {
                    mode = cof.Mode;
                    width = cof.Width;
                    heigth = cof.Heigth;
                    type = cof.Type;
                    
                    OnImageEvent(this);
                    //this.Save();
                    return;
                }
            }
        }

        private void OnImageEvent(ImageConfig e)
        {
            if (ImageSetEvent != null)
            { // 如果有对象注册
                ImageSetEvent(e);  // 调用所有注册对象的方法
            }
        }

        public void Save()
        {
            string xmlstr = XmlUtil.Serializer(typeof(ImageConfig), this);
            xml.LoadXml(xmlstr);
            xml.Save(FILE_NAME);
        }

        public ImageConfig Load()
        {
            xml.Load(FILE_NAME);
            ImageConfig stu2 = XmlUtil.Deserialize(typeof(ImageConfig), xml.InnerXml) as ImageConfig;
            return stu2;
        }

    }

    public class XmlUtil
    {
        #region 反序列化
        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="type">类型</param>
        /// <param name="xml">XML字符串</param>
        /// <returns></returns>
        public static object Deserialize(Type type, string xml)
        {
            try
            {
                using (StringReader sr = new System.IO.StringReader(xml))
                {
                    System.Xml.Serialization.XmlSerializer xmldes = new System.Xml.Serialization.XmlSerializer(type);
                    return xmldes.Deserialize(sr);
                }
            }
            catch (Exception)
            {

                return null;
            }
        }
        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="type"></param>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static object Deserialize(Type type, Stream stream)
        {
            System.Xml.Serialization.XmlSerializer xmldes = new System.Xml.Serialization.XmlSerializer(type);
            return xmldes.Deserialize(stream);
        }
        #endregion

        #region 序列化
        /// <summary>
        /// 序列化
        /// </summary>
        /// <param name="type">类型</param>
        /// <param name="obj">对象</param>
        /// <returns></returns>
        public static string Serializer(Type type, object obj)
        {
            MemoryStream Stream = new MemoryStream();
            System.Xml.Serialization.XmlSerializer xml = new System.Xml.Serialization.XmlSerializer(type);
            try
            {
                //序列化对象
                xml.Serialize(Stream, obj);
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            Stream.Position = 0;
            StreamReader sr = new StreamReader(Stream);
            string str = sr.ReadToEnd();

            sr.Dispose();
            Stream.Dispose();

            return str;
        }

        #endregion
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    public class ImageConfig
    {

        #region 字段信息
        private const string FILE_NAME = @"ImageConfig.config";
        private XmlDocument xml = new XmlDocument();
        //图片宽度
        private int width;
        public int Width { get { return width; } set { width = value; } }
        //图片高度
        private int heigth;
        public int Heigth { get { return heigth; } set { heigth = value; } }
        //输出路径
        private string outpath;
        public string OutPath { get { return outpath; } set { outpath = value; } }
        //类型  存在1寸循环铺盖所以加了类型来判断
        private int type;
        public int Type { get { return type; } set { type = value; } }
        //名称
        private string text;
        public string Text { get { return text; } set { text = value; } }
        //布局  居中或裁剪
        private int layout = 0;
        public int Layout { get { return layout; } set { layout = value; } }
        //事件委托
        public delegate void ImageSetEventHandler(ImageConfig e);
        public event ImageSetEventHandler ImageSetEvent; //声明事件        
        #endregion

        #region 设置图片大小
        /// <summary>
        /// 设置图片大小
        /// 使用方法：clss.SetPrintPattern("6寸","有边框");
        /// </summary>
        /// <param name="Pattern">打印尺寸</param>
        /// <param name="Margin">边框</param>
        public void SetImageSize(string Pattern)
        {
            //XmlNode PaperSizes = this.xml.SelectSingleNode("PrintImageConfig/PaperSizes");
            XmlNodeList nodeList = this.xml.SelectSingleNode("ImageConfig/ImageSizes").ChildNodes;//获取PaperSizes节点的所有子节点 
            foreach (XmlNode xn in nodeList)//遍历所有子节点 
            {
                XmlElement xe = (XmlElement)xn;//将子节点类型转换为XmlElement类型 
                if (xe.InnerText == Pattern)
                {
                    this.width = Int32.Parse(xe.GetAttribute("width"));
                    this.heigth = Int32.Parse(xe.GetAttribute("heigth"));
                    this.type = Int32.Parse(xe.GetAttribute("type"));
                    this.text = xe.InnerText;
                    //保存到配置文件
                    this.Save();
                    OnImageEvent(this);
                    return;
                    //break;
                }
            }
           // this.Save();
        }

        private void OnImageEvent(ImageConfig e){
            if (ImageSetEvent != null)
            { // 如果有对象注册
                ImageSetEvent(e);  // 调用所有注册对象的方法
            }       
        }
        #endregion

        #region 载入配置
        /// <summary>
        /// 加载xml文件配置
        /// </summary>
        public void LoadData()
        {
            //图片宽度
            XmlNode DefaultImageSize = this.xml.SelectSingleNode("ImageConfig/DefaultImageSize");
            XmlElement xe = (XmlElement)DefaultImageSize;
            this.width = Int32.Parse(xe.GetAttribute("width"));
            //图片高度
            this.heigth = Int32.Parse(xe.GetAttribute("heigth"));
            //类型
            this.type = Int32.Parse(xe.GetAttribute("type"));
            //图片路径
            XmlNode OutPath = this.xml.SelectSingleNode("ImageConfig/OutPath");
            this.outpath = OutPath.InnerText;
            //内容
            this.text = DefaultImageSize.InnerText;

        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public ImageConfig()
        {
            try
            {
                xml.Load(FILE_NAME);
                this.LoadData();
            }
            catch (DirectoryNotFoundException)
            {
                throw new DirectoryNotFoundException("图片配置文件不存在");
            }

        }
        #endregion

        #region 保存数据
        /// <summary>
        /// 保存配置文件
        /// </summary>
        public void Save()
        {
            //图片宽度
            XmlNode DefaultImageSize = this.xml.SelectSingleNode("ImageConfig/DefaultImageSize");
            XmlElement xe = (XmlElement)DefaultImageSize;
            xe.SetAttribute("width",this.width.ToString());
            //图片高度
            xe.SetAttribute("heigth", this.heigth.ToString());
            //类型
            xe.SetAttribute("type", this.type.ToString());
            //xmltext
            xe.InnerText = this.text;
            //图片路径
            XmlNode OutPath = this.xml.SelectSingleNode("ImageConfig/OutPath");
            OutPath.InnerText = this.outpath;
            this.xml.Save(FILE_NAME);
        }
        #endregion
    }
    */
    
}
