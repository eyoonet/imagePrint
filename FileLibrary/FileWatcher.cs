﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
///        调用列子：
///        private void button1_Click(object sender, EventArgs e)
///        {
///            FileWatcher a = new FileWatcher(@"F:\");
///            a.Start();
///            a.Boiled += aaa;   //注册方法
///        }
///        public void aaa(FileSystemEventArgs e){
///
///            textBox1.Text = e.Name;
///        }
namespace FileLibrary
{
    public class  FileWatcher
    {
        #region 字段信息
        //监视路径
        private string watcherPath;
        public string WatcherPath 
        { 
            get { return watcherPath; } 
            set { watcherPath = value; }
        }
        public bool Service 
        { 
            get { return watcher.EnableRaisingEvents; } 
            set { watcher.EnableRaisingEvents = value; }
        }
        public delegate void CreatedFileEventHandler(FileSystemEventArgs e);
        public event CreatedFileEventHandler CreatedFileEventArgs; //声明事件
        FileSystemWatcher watcher = new FileSystemWatcher();
        #endregion

        #region 事件方法
        protected virtual void OnBoiled(FileSystemEventArgs e)
        {
            if (CreatedFileEventArgs != null)
            { // 如果有对象注册
                CreatedFileEventArgs(e);  // 调用所有注册对象的方法
            }
        }
        #endregion

        #region 构造函数
        public FileWatcher(string path) {
            if (path == string.Empty) 
            {
                path = @"D:\";
            }
            watcherPath = path;
            SetWatcher();
            watcher.EnableRaisingEvents = true;
        }
        #endregion

        #region 文件监视设置
        public  void SetWatcher()
        {
            //监视路径
            watcher.Path = watcherPath;
            //指定要在文件或文件夹中监视的更改  。文件名。目录名。
            watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName;
            //获取或设置筛选字符串，用于确定在目录中监视哪些文件。
            watcher.Filter = "*.*"; //"图片|*.JPG;*.png;*.gif";
            //添加事件处理函数
            // watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            //watcher.Deleted += new FileSystemEventHandler(OnChanged);
            //watcher.Renamed += new RenamedEventHandler(OnRenamed);
            // Begin watching. true 开始  false 停止；
            
        }
        #endregion、

        #region 文件创建被触发函数
        private void OnChanged(object source, FileSystemEventArgs e)
        {
            string extstr = "bmp|jpg|jpeg|png|gif";
            string[] extstrs = extstr.Split(new Char[] {'|'});//分割字符串
            string[] exts = e.Name.Split(new Char[] {'.'});//分割取出发现的文件名扩展
            if (1 == exts.Length) return;//没有扩展名就直接返回
            string ext = exts[exts.Length - 1].ToLower();//得到扩展名不管是大写还是小写转换到小写
            for (int i = 0; i < extstrs.Length; i++ )
            {
                if (ext == extstrs[i])
                {
                    OnBoiled(e);  // 调用 OnBolied方法
                }
            }
            //Console.WriteLine("File: " + e.FullPath + " " + e.ChangeType);
        }
        #endregion

        #region 文件删除触发
        private void OnRenamed(object source, RenamedEventArgs e)
        {
            // Specify what is done when a file is renamed.
            //Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath);
        }
        #endregion
    }
}
